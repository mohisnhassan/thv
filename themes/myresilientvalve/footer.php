</main>
<footer class="footer mt-auto py-5 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center text-lg-left my-auto"><span class="text-white">&copy; <script>document.write(new Date().getFullYear())</script> Edwards Lifesciences Corporation. All rights reserved.</span>
                <ul class="list-unstyled list-inline">
                        <li class="list-inline-item text-white"><a class="nav-link pl-0" target="_blank" href="https://www.edwards.com/legal/privacypolicy">Privacy Policy</a></li>
                        <li class="list-inline-item text-white"><a class="nav-link mr-0" href="/legal-terms">Legal Terms</a></li>
                    </ul>
                    <div id="teconsent" class="mb-4 mb-lg-0"></div>
                </div>
                <div class="col-lg-4 text-center text-lg-right">
                    <img src="https://www.edwards.com/Themes/NewBrand/Styles/Images/edwards/logo-edwards-footer.png" alt="Edwards Lifesciences Logo" class="img-fluid">
                </div>
            </div>
        </div>
        <?php wp_footer() ?>
    </footer>
</body>

</html>