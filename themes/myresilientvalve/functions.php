<?php 

function load_styles() {
    wp_enqueue_style( 'styles', get_stylesheet_uri() );
    wp_enqueue_style( 'material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css');
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css');
    wp_enqueue_style( 'google fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');
    wp_enqueue_style('Merriweather-Sans','https://fonts.googleapis.com/css?family=Merriweather+Sans:300,400,700&display=swap');
}

add_action( 'wp_enqueue_scripts', 'load_styles' ); 

function load_scripts() {

    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery'));
    wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('aos', 'https://unpkg.com/aos@next/dist/aos.js');
    wp_enqueue_script('app', get_stylesheet_directory_uri() . '/app.js', array('jquery'));
}

add_action( 'wp_enqueue_scripts', 'load_scripts',-1);

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'myresilientvalve' ),
) );

remove_filter( 'the_content', 'wpautop' );

//Remove WPAUTOP from ACF TinyMCE Editor
function acf_wysiwyg_remove_wpautop() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'acf_wysiwyg_remove_wpautop');

function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
	
	// remove layout title from text
	$title = '';
	
	
	// load text sub field
	if( $text = get_sub_field('text') ) {
		
		$title .= $text;
		
	}
	
	// return
	return $title;
	
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);


// ACF Blocks
function register_acf_block_types() {

    // register a testimonial block.
    acf_register_block_type(array(
        'name'              => 'testimonial',
        'title'             => __('Testimonial'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
        'enqueue_style'     => get_stylesheet_uri(),
        'category'          => 'formatting',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'testimonial', 'quote' ),
    ),

);
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}

?>