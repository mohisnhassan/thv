<?php

get_header() ?>


    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
           <?php // check if the flexible content field has rows of data
				if( have_rows('flexible_content') ): while ( have_rows('flexible_content') ) : the_row(); ?>

				      <?php if( get_row_layout() == 'html' ): ?>

				        	<?php the_sub_field('html'); ?>

				      <?php  endif;?>

					  <?php if( get_row_layout() == 'scripts' ): ?>

				        	<?php the_sub_field('scripts'); ?>

				      <?php  endif;?>
				<?php endwhile; endif;?>
	<?php endwhile; endif; ?>


<?php get_footer() ?>