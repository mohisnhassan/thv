jQuery(function ($) {

    // Accordion

    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-heading').addClass('active');
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-heading').removeClass('active');
    });


    // Rotate icon

    $("a").click(function () {
        $('.rotate').toggleClass("down");
    });

    // Form validation script

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();


    $('#exampleModal').modal('handleUpdate');

    //Animation Options 

    AOS.init({
        once: true
    });

});

// Reduce size of navbar brand on page scroll

window.onscroll = function () {
    let windowWidth = window.innerWidth;
    if (windowWidth > 1310) {
        scrollFunction();
    }
};

function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.querySelector(".navbar-brand img").style.transform = "scale(0.8,0.8)";
        document.querySelector(".navbar-brand img").style.transition = "transform 0.2s";
        document.querySelector(".navbar-brand").style.top = "-33px";
        document.getElementById('navbar').style.padding = "2rem 1rem";
    } else {
        document.querySelector(".navbar-brand img").style.transform = "scale(1,1)";
        document.querySelector(".navbar-brand").style.transition = "left 0.2s";
        document.querySelector(".navbar-brand").style.transition = "top 0.2s";
        document.querySelector("#navbar").style.transition = "padding 0.2s";
        document.querySelector(".navbar-brand img").style.transition = "transform 0.2s";
        document.getElementById('navbar').style.padding = "2.5rem";
        document.querySelector(".navbar-brand").style.top = "-7px";
    }
}