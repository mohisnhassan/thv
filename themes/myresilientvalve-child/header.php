<!doctype html>
<html lang="en" class="h-100">

<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MVLDZTR');</script>
<!-- End Google Tag Manager -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://www.edwards.com/Themes/NewBrand/Styles/images/edwards/favicon_edwards.ico" rel="shortcut icon" type="image/x-icon">
    <title><?php wp_title(' | ', 'echo', 'right'); ?><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?> 
</head>

<body data-aos="fade-in" <?php body_class('h-100 d-flex flex-column') ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MVLDZTR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="consent_blackbar"></div> 
<script async="async" src="//consent.trustarc.com/notice?domain=edwards_ccpa.com&c=teconsent&js=nj&noticeType=bb" crossorigin></script>

    <main role="main" class="flex-shrink-0">
    <nav id="navbar" class="navbar fixed-top navbar-expand-lg navbar-light">
        <div class="container"><a class="navbar-brand" href="<?php echo home_url()?>"><img src="/wp-content/uploads/2019/09/logo-edwards.png" alt="Edwards Lifesciences Logo"></a>
            <?php
                wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                    'container'       => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'bs-example-navbar-collapse-1',
                    'menu_class'      => 'navbar-nav ml-auto text-white',
                    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'          => new WP_Bootstrap_Navwalker(),
                ) ); ?>
        </div>
    </nav>